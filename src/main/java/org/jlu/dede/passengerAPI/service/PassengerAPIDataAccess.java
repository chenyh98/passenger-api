package org.jlu.dede.passengerAPI.service;

import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
@FeignClient(name = "data-access",path = "10.100.0.2:8765")
public interface PassengerAPIDataAccess {

    @GetMapping("/drivers/order/{id}")
    @ResponseBody
    Driver findByOrder(@PathVariable Integer id);
}

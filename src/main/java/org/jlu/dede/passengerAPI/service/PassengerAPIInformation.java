package org.jlu.dede.passengerAPI.service;

import org.jlu.dede.publicUtlis.model.Account;
import org.jlu.dede.publicUtlis.model.BlackList;
import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Service
@FeignClient(name = "service-information",url = "10.100.0.5:8769")
public interface PassengerAPIInformation {

    //输入邮箱点击确定（发送验证码）
    @RequestMapping(value = "/passenger/register/send",method = RequestMethod.POST)
    boolean  registerSend(@RequestParam(value = "emailAccount") String emailAccount);

    //输入验证码和密码点击确定（验证验证码并输入密码）
    @RequestMapping(value = "/passenger/register/verify",method = RequestMethod.POST)
    boolean  registerVerify(@RequestParam (value = "verifyCode")String verifyCode,@RequestParam (value = "password")String password,@RequestParam(value = "emailAccount") String emailAccount);


    //输入邮箱密码点击登录
    @RequestMapping(value = "/passenger/logIn",method = RequestMethod.POST)
    ResponseEntity<String> logIn(@RequestParam String emailAccount, @RequestParam String password);

    //登出
    @RequestMapping(value = "/passenger/logOut",method = RequestMethod.POST)
    ResponseEntity<String> logOut(@RequestParam String token);

    //验证身份
    @RequestMapping(value = "/passenger/verification",method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> verification(@RequestParam String token);


    //修改完后点击保存（编辑个人资料）
    @RequestMapping(value = "/passenger/editUserData/{id}", method = RequestMethod.POST)
    boolean editUserData(@PathVariable Integer id, @RequestBody Passenger passenger);


    //点击个人信息按钮（查询个人资料）
    @RequestMapping(value = "/passenger/queryUserData", method = RequestMethod.GET)
    Passenger queryUserData(@RequestParam Integer id);

    @RequestMapping(value = "/passenger/queryPassengerAccount", method = RequestMethod.GET)
    public Account queryPassengerAccount(@RequestParam(value = "id") Integer id);

    //点击黑名单按钮（查询黑名单）
    @RequestMapping(value = "/passenger/queryBlacklist", method = RequestMethod.GET)
    List<BlackList> queryBlackList(@RequestParam(value = "id") Integer id);

    //在历史订单中拉黑司机（增加黑名单）
    @RequestMapping(value = "/passenger/addBlacklist/{id}", method = RequestMethod.POST)
    boolean addBlackList(@PathVariable Integer id, @RequestBody Driver driver);

    //在查看黑名单时删除（删除黑名单）
    @RequestMapping(value = "/passenger/deleteBlacklist/{id1}/{id2  }", method = RequestMethod.DELETE)
    boolean deleteBlackList(@PathVariable Integer id1,@PathVariable Integer id2);
}

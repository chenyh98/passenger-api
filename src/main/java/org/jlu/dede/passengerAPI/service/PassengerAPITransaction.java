package org.jlu.dede.passengerAPI.service;

import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Record;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

@Service
@FeignClient(name = "service-deal" ,url="10.100.0.4:8767")
public interface PassengerAPITransaction {

    //付钱
    @RequestMapping("/passenger/deal/payOrder")
    boolean payOrder(@RequestBody Order order);

    //充值
    @RequestMapping(value = "/passenger/deal/recharge",method = RequestMethod.POST)
    boolean recharge(@RequestParam(value = "id")Integer id, @RequestParam(value = "money")BigDecimal money);

    //通过乘客ID查询交易记录
    @RequestMapping(value = "/passenger/deal/query",method = RequestMethod.GET)
    List<Record> findByPayer(@RequestParam(value = "id") Integer id);

    //通过交易记录ID查询
    @RequestMapping(value="/passenger/record/deal/query",method = RequestMethod.GET)
    Record findByRecord(@RequestParam(value = "id")Integer id);

}

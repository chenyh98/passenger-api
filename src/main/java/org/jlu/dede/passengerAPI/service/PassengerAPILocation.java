package org.jlu.dede.passengerAPI.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(name = "service-location",url = "10.100.0.6:8770")
public interface PassengerAPILocation {
    //更新乘客实时位置
    @RequestMapping(value = "/location/updatePassengerLocation",method = RequestMethod.POST)
    boolean updateLocationPassenger(@RequestParam(value ="x")String x, @RequestParam(value ="y") String y, @RequestParam(value ="id") Integer id);

    //；10s刷新为乘客推送周围司机位置
    @RequestMapping(value = "/location/findNowSiteDriver", method = RequestMethod.POST)
    boolean findNowSiteDriver(@RequestParam(value = "x") String x, @RequestParam(value = "y")String y,@RequestParam(value = "id") Integer id);

}

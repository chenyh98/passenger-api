package org.jlu.dede.passengerAPI.service;

import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@FeignClient(name = "service-order",url = "10.100.0.1:8768")
public interface PassengerAPIOrder {

    //点击发起当前订单(创建当前订单)
    @RequestMapping(value = "/order/passenger/addCurrentOrder/{x}/{y}", method = RequestMethod.POST)
    Integer addCurrentOrder(@RequestBody Order order, @PathVariable String x, @PathVariable String y);


    //点击发起预约订单（创建预约订单）
    @RequestMapping(value = "/order/passenger/addBookingOrder/{x}/{y}", method = RequestMethod.POST)
    Integer addBookingOrder(@RequestBody Order order, @PathVariable String x, @PathVariable String y);

    //查看当前订单
    @RequestMapping(value ="/order/passenger/queryCurrentOrder", method = RequestMethod.POST)
    Order queryCurrentOrder(@RequestParam(value = "id") Integer id);

    //查看预约订单
    @RequestMapping(value ="/order/passenger/queryBookingOrder", method = RequestMethod.POST)
    List queryBookingOrder(@RequestParam(value = "id") Integer id);

    //取消预约订单
    @RequestMapping(value = "/order/passenger/cancelBookingOrder",method = RequestMethod.POST)
    boolean cancelBookingOrder(@RequestBody Order order);

    //取消当前订单
    @RequestMapping(value ="/order/passenger/cancelCurrentOrder", method = RequestMethod.POST)
    boolean cancelCurrentOrder(@RequestParam("id") Integer id);


    //乘客结束旅途
    @RequestMapping(value = "/order/passenger/arriveDestination",method = RequestMethod.POST)
    boolean arriveDestination(@RequestParam Integer id);

    //查询历史订单
    @RequestMapping(value ="/order/passenger/queryHistoryOrder", method = RequestMethod.POST)
    List queryHistoryOrder(@RequestParam(value = "id") Integer id);

    //添加评价
    @RequestMapping(value ="/order/passenger/addEvaluation", method = RequestMethod.POST)
    boolean addEvaluation(@RequestBody Order order);

}

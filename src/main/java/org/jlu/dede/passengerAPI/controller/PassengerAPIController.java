package org.jlu.dede.passengerAPI.controller;

import org.jlu.dede.passengerAPI.service.*;
import org.jlu.dede.publicUtlis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/passenger")
public class PassengerAPIController {

    @Autowired
    PassengerAPIInformation passengerAPIInformation;
    @Autowired
    PassengerAPIOrder passengerAPIOrder;
    @Autowired
    PassengerAPITransaction passengerAPITransaction;
    @Autowired
    PassengerAPILocation passengerAPILocation;
    @Autowired
    PassengerAPIDataAccess passengerAPIDataAccess;



    //<!---------------信息维护部分----------->
    //输入邮箱点击确定（发送验证码）
    //验证完毕
    @RequestMapping(value = "/register/send",method = RequestMethod.POST)
    @ResponseBody
    public boolean  registerSend(@RequestParam String emailAccount){
        return passengerAPIInformation.registerSend(emailAccount);
    }

    //输入验证码和密码点击确定（验证验证码并输入密码）
    //验证完毕
    @RequestMapping(value = "/register/verify",method = RequestMethod.POST)
    @ResponseBody
    public boolean  registerVerify(@RequestParam String verifyCode,@RequestParam String password,@RequestParam String emailAccount){
        return passengerAPIInformation.registerVerify(verifyCode,password,emailAccount);
    }

    //输入邮箱密码点击登录
    //验证完毕
    @RequestMapping(value = "/logIn",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> logIn(String emailAccount, String password)
    {
        return  passengerAPIInformation.logIn(emailAccount,password);
    }

    //登出
    //验证完毕
    @RequestMapping(value = "/logOut",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> logOut(String token){
        return passengerAPIInformation.logOut(token);
    }

    //验证身份
    //验证完毕
    @RequestMapping(value = "/verification",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> verification(@RequestParam String token){
       return  passengerAPIInformation.verification(token);
    }

    //修改完后点击保存（编辑个人资料）
    //验证完毕(必须先查询个人信息后才可以编辑)
    @RequestMapping(value = "/editUserData", method = RequestMethod.POST)
    @ResponseBody
    public boolean editUserData(@RequestHeader("user_id")String id,@RequestBody Passenger passenger)
    {
        return passengerAPIInformation.editUserData(Integer.parseInt(id),passenger);
    }

    //查询个人账户
    @RequestMapping(value = "/queryPassengerAccount", method = RequestMethod.GET)
    @ResponseBody
    public Account queryPassengerAccount(@RequestHeader("user_id")String id){

        return passengerAPIInformation.queryPassengerAccount(Integer.parseInt(id));
    }

    //点击个人信息按钮（查询个人资料）
    //验证完毕
    @RequestMapping(value = "/queryUserData", method = RequestMethod.GET)
    @ResponseBody
    public Passenger queryUserData(@RequestHeader("user_id")String id){
        return passengerAPIInformation.queryUserData(Integer.parseInt(id));
    }

    //点击黑名单按钮（查询黑名单）
    //验证完毕
    @RequestMapping(value = "/queryBlackList", method = RequestMethod.GET)
    @ResponseBody
    public List<BlackList> queryBlackList(@RequestHeader("user_id")String id)
    {
        return passengerAPIInformation.queryBlackList(Integer.parseInt(id));
    }

    //在历史订单中拉黑司机（增加黑名单）
    //验证完毕（1多次添加黑名单重复  2前端必须完整传送一个driver的JSON，必须包含ID，其他 可有可无）
    @RequestMapping(value = "/addBlacklist", method = RequestMethod.POST)
    @ResponseBody
    public boolean addBlackList(@RequestHeader("user_id")String id, @RequestBody Driver driver)
    {
        return passengerAPIInformation.addBlackList(Integer.parseInt(id),driver);
    }

    //在查看黑名单时删除（删除黑名单）
    //验证完毕
    @RequestMapping(value = "/deleteBlacklist/{id2}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteBlackList(@RequestHeader("user_id")String id,@PathVariable Integer id2)
    {
        return passengerAPIInformation.deleteBlackList(Integer.parseInt(id),id2);
    }


    //<!--------订单管理部分-------------->
    //点击发起当前订单(创建当前订单)
    //验证完毕
    //网关完毕
    @PostMapping("/addCurrentQuickOrder/{x}/{y}")
    @ResponseBody
    public Integer addCurrentQuickOrder(@RequestHeader("user_id") String id,@PathVariable String x, @PathVariable String y,@RequestParam String idealdest,@RequestParam String idealdepart,@RequestParam String date){
        Order order1 =new Order();
        Passenger passenger = new Passenger();
        passenger.setId(Integer.parseInt(id));
        order1.setPassenger(passenger);
        order1.setType(1);
        order1.setState(0);
        order1.setIdealDestinationSite(idealdest);
        order1.setIdealDepartSite(idealdepart);
        order1.setCreateTime(date);
        return passengerAPIOrder.addCurrentOrder(order1,x,y);
    }

    @PostMapping("/addCurrentSpecificOrder/{x}/{y}")
    @ResponseBody
    public Integer addCurrentSpecificOrder(@RequestHeader("user_id") String id,@PathVariable String x, @PathVariable String y,@RequestParam String idealdest,@RequestParam String idealdepart,@RequestParam String date){
        Order order1 =new Order();
        Passenger passenger = new Passenger();
        passenger.setId(Integer.parseInt(id));
        order1.setPassenger(passenger);
        order1.setType(2);
        order1.setState(0);
        order1.setIdealDestinationSite(idealdest);
        order1.setIdealDepartSite(idealdepart);
        order1.setCreateTime(date);
        return passengerAPIOrder.addCurrentOrder(order1,x,y);
    }
    //点击发起预约订单（创建预约订单）
    //验证完毕
    //订单类型出错，异常无法接收。
    @PostMapping("/addBookingOrder/{x}/{y}")
    @ResponseBody
    public Integer addBookingOrder(@RequestHeader("user_id")String id, @PathVariable String x, @PathVariable String y,@RequestParam String idealdest,@RequestParam String idealdepart,@RequestParam String date){
        Order order1 =new Order();
        Passenger passenger = new Passenger();
        passenger.setId(Integer.parseInt(id));
        order1.setPassenger(passenger);
        order1.setType(3);
        order1.setState(0);
        order1.setIdealDestinationSite(idealdest);
        order1.setIdealDepartSite(idealdepart);
        order1.setOrderTime(date);
        return passengerAPIOrder.addBookingOrder(order1,x,y);
    }

    //查看当前订单
    //验证完毕
    @RequestMapping("/queryCurrentOrder")
    @ResponseBody
    public Order queryCurrentOrder(@RequestHeader("user_id") String  id){
        return passengerAPIOrder.queryCurrentOrder(Integer.parseInt(id));
    }

    //查看预约订单
    //验证完毕
    @RequestMapping("/queryBookingOrder")
    @ResponseBody
    public List queryBookingOrder(@RequestHeader("user_id") String  id) {
        return passengerAPIOrder.queryBookingOrder(Integer.parseInt(id));
    }

    //取消预约订单
    //验证完毕
    @RequestMapping(value = "/cancelBookingOrder",method = RequestMethod.POST)
    @ResponseBody
    public boolean cancelBookingOrder(@RequestParam Integer orderID) {
        Order order1 =new Order();
        order1.setId(orderID);
        return passengerAPIOrder.cancelBookingOrder(order1);
    }

    //取消当前订单
    //验证完毕
    @RequestMapping("/cancelCurrentOrder")
    public boolean cancelCurrentOrder(@RequestHeader("user_id") String  id) {
        return passengerAPIOrder.cancelCurrentOrder(Integer.parseInt(id));
    }

    //乘客结束旅途
    //验证完毕
    @RequestMapping(value = "/arriveDestination",method = RequestMethod.POST)
    @ResponseBody
    public boolean arriveDestination(@RequestHeader("user_id") String  id){
        return passengerAPIOrder.arriveDestination(Integer.parseInt(id));
    }

    //查询历史订单
    //验证完毕
    @RequestMapping("/queryHistoryOrder")
    @ResponseBody
    public List queryHistoryOrder(@RequestHeader("user_id") String  id){
        return passengerAPIOrder.queryHistoryOrder(Integer.parseInt(id));
    }

    //添加评价
    //验证完毕
    @RequestMapping("/addEvaluation")
    @ResponseBody
    public boolean addEvaluation(@RequestParam Integer orderID,double evaluation) {
        Order order=new Order();
        order.setId(orderID);
        order.setVetifyValue(evaluation);
        return passengerAPIOrder.addEvaluation(order);
    }

    //<!-------------交易部分--------------->
    @RequestMapping(value = "/deal/payOrder",method = RequestMethod.POST)
    @ResponseBody
    public boolean payOrder(@RequestParam("orderID") String orderID,@RequestParam("state") Integer state,@RequestParam("money") BigDecimal money){
        Order order=new Order();
        order.setId(Integer.valueOf(orderID));
        order.setState(state);
        order.setFinalMoney(money);
        return passengerAPITransaction.payOrder(order);
    }

    //充值
    //验证完毕
    @RequestMapping(value = "/deal/recharge",method = RequestMethod.POST)
    @ResponseBody
    public boolean recharge(@RequestHeader("user_id") String id, @RequestParam(value = "money") BigDecimal money){
        return passengerAPITransaction.recharge(Integer.parseInt(id),money);
    }

    //通过乘客ID查询交易记录
    //验证完毕
    @RequestMapping(value = "/deal/query",method = RequestMethod.GET)
    public List<Record> findByPayer(@RequestHeader("user_id") String id )
    {
        return passengerAPITransaction.findByPayer(Integer.parseInt(id));
    }

//    //通过交易记录ID查询
//    //验证完毕
//    @RequestMapping(value="/record/deal/query",method = RequestMethod.GET)
//    public Record findByRecord(@RequestHeader("user_id") String id )
//    {
//        return passengerAPITransaction.findByRecord(Integer.parseInt(id));
//    }

    //<!-------------------更新位置信息-------------->

    //；10s刷新为乘客推送周围司机位置
    //乘客的id
    @RequestMapping(value = "/findNowSiteDriver",method = RequestMethod.POST)
    public boolean findNowSiteDriver(@RequestParam(value ="x")String x,@RequestParam(value ="y")String y,@RequestHeader("user_id") Integer id) {
        return passengerAPILocation.findNowSiteDriver(x,y,id);
    }

    //此处ID是order的ID
    @GetMapping("/drivers/order/{id}")
    public String findByOrder(@PathVariable String id){
       // return "Hello World";
        return passengerAPIDataAccess.findByOrder(Integer.valueOf(id)).getId().toString();
    }


}
